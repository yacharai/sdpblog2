class SessionsController < ApplicationController
  def new
  end

  def create
  	user = User.find_by_email(params[:email])
  	if user && user.authenticate(params[:password])
  		session[:user_id] = user.id
  		redirect_to new_dashboard_path, notice: 'Logged in !'
  	else
  		redirect_to login_path, notice: 'Incorrect username or password !'

  	end
  end
  def destroy 
  	session[:user_id] = nil
  	redirect_to root_url, notice: 'Logged out !'
  end
end
